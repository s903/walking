package person;

public class Person {
    private int age;
    private double money;
    private boolean drunk;
    private int strenght;
    public int stamina = 100;

    public Person(int age){
        this.age = age;
    }

    public void getStronger(){
        strenght++;
        stamina -= 10;
    }

    public void work(){
        stamina -= 10;
    }

    public int getStamina() {
        return stamina;
    }

    public void rest(){
        stamina = 100;
    }

    public int getAge() {
        return age;
    }

    public void setDrunk(boolean drunk){
        this.drunk = drunk;
    }

    public boolean isDrunk(){
        return drunk;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", money=" + money +
                ", drunk=" + drunk +
                '}';
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getMoney(){return money;
    }
}