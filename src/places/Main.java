package places;
import person.Person;
import places.Bar;
public class Main {
    public static void main(String[] args){

        Person person = new Person(20);
        person.setMoney(100);

        System.out.println(person.getAge());

        Bar bar = new Bar();
        bar.visit(person);

        System.out.println(person.toString());

        Gym gym = new Gym(); //izveidots objekts no metodes
        gym.visit(person);

    }
}
