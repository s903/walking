package places;

import person.Person;

public class Work {
    private int workHours = 8;
    public void visit(Person p){
        for(int i = 0; i<workHours; i++){
            if(p.getStamina()<=0){
                break;
            }
            p.setMoney(p.getMoney() + 10);
        }
    }
}
